# SensorThings API Dashbord 
[Demo](http://nunknown.com/sensorthingsdashbord/index.html)
# Step 1: Install AnjularJS Admin Dashbord
[Dashbord](http://startangular.com/product/sb-admin-angular-theme/)
Make sure you have [bower](http://bower.io/), [grunt-cli](https://www.npmjs.com/package/grunt-cli) and  [npm](https://www.npmjs.org/) installed globally
On the command prompt run the following commands
- cd `project-directory`
- `npm install` 
- `npm start`
- `npm run dist` 

# Step 2: Include these libraries as follows:
```javascript
 	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.10/angular-ui-router.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-animate.min.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
    <script src="js/sensorthings.js"></script>   
	<script src="js/app.js"></script>
```
## Now all controllers are put in the app.js. It should be put in their own module's folder in the future. 
## SensorThings.js is a simple library to perform CRUD actions on the SensorThings entity types.
### Example: Create Thing
```javascript
	var jsondata = {"description":"This is a Thing"};
 	SensorThings.Thing.new(jsondata);
```

## The demo folder contains a single web application without using SB Admin Dashbord.

