'use strict';

angular.module("sbAdminApp", ["oc.lazyLoad", "ui.router", "ui.bootstrap", "angular-loading-bar","leaflet-directive"]).config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider",
function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: !1,
        events: !0
    }),
    $urlRouterProvider.otherwise("/dashboard/thing"),
    $stateProvider.state("dashboard", {
        url: "/dashboard",
        templateUrl: "views/dashboard/main.html",
        resolve: {
            loadMyDirectives: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "sbAdminApp",
                    files: ["scripts/directives/header/header.js", "scripts/directives/header/header-notification/header-notification.js", "scripts/directives/sidebar/sidebar.js", "scripts/directives/sidebar/sidebar-search/sidebar-search.js"]
                }),
                $ocLazyLoad.load({
                    name: "toggle-switch",
                    files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js", "bower_components/angular-toggle-switch/angular-toggle-switch.css"]
                }),
                $ocLazyLoad.load({
                    name: "ngAnimate",
                    files: ["bower_components/angular-animate/angular-animate.js"]
                })
            }
        }     
    }).state("dashboard.thing", {
        url: "/thing",
        controller: "thingControl",
        templateUrl: "views/form-thing.html"
    }).state("dashboard.content", {
        templateUrl: "views/form-content.html",
        url: "/content",
        controller:"contentControl"
    }).state("dashboard.submit", {
        templateUrl: "views/form-submit.html",
        url: "/submit",
        controller:"submitControl"
    }).state("dashboard.display", {
        templateUrl: "views/form-display.html",
        url: "/display",
        controller: "displayControl",
        resolve: {
            loadMyFile: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "chart.js",
                    files: ["bower_components/angular-chart.js/dist/angular-chart.min.js", "bower_components/angular-chart.js/dist/angular-chart.css"]
                })
            }
        }
    })
}]);