angular.module('sbAdminApp').
    controller('contentControl', function($scope, Thing) {
        angular.extend($scope, {
            calgary: {
                lat: 51.079948,
                lng: -114.125534,
                zoom: 4
            },
            markers: {
                m: {
                    lat: 51.079948,
                    lng: -114.125534
                }
            },
            events: {}
        });

    Thing.formData.location = $scope.markers.m;
    
    $scope.$on("leafletDirectiveMap.click", function(event, args){
        var leafEvent = args.leafletEvent;

        $scope.markers.m.lat = leafEvent.latlng.lat;
        $scope.markers.m.lng = leafEvent.latlng.lng;

        Thing.formData.location = leafEvent.latlng;
    });

    $scope.formData = {};
    $scope.formData.Thing_url = Thing.formData.Thing_url;
    $scope.mapState = {
        show: true
    };

    $scope.dataStreamState = {
        show: false
    };

    $scope.showMap = function() {
        $scope.mapState.show = !$scope.mapState.show;
    };

    $scope.showAddDataStream = function() {
        $scope.dataStreamState.show = !$scope.dataStreamState.show;
    };

    $scope.allUnit = [
        {value: 0, unitName: 'Please choose unit'},
        {value: "Fahrenheit", unitName: 'Fahrenheit'},
        {value: "Celsius", unitName: 'Celsius'},
        {value: "Knot", unitName: 'Knot'},
        {value: "Kilometer", unitName: 'Kilometer'},
        {value: "Meter", unitName: 'Meter'},
        {value: "Centimeter", unitName: 'Centimeter'},
        {value: "Millimeter", unitName: 'Millimeter'},
        {value: "Mile", unitName: 'Mile'},
        {value: "Celsius", unitName: 'Celsius'},
        {value: "Inch", unitName: 'Inch'},
        {value: "Percent", unitName: 'Percent'}
    ];

    $scope.allProperty = [
        {value: 0, propertyName: 'Please choose property'},
        {value: "Humidity", propertyName: 'Humidity'},
        {value: "DewPoint Temperature", propertyName: 'DewPoint Temperature'},
        {value: "Room Temperature", propertyName: 'Room Temperature'},
        {value: "Precipitation", propertyName: 'Precipitation'},
        {value: "Wind Speed", propertyName: 'Wind Speed'},
        {value: "Atmospheric Pressure", propertyName: 'Atmospheric Pressure'}
    ];

    $scope.data = {
        currentUnit:0,
        currentProperty:0
    };

    $scope.formData.definition = "";

    var owl = "https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/WeatherOntology.owl#";
   
    $scope.switchProperty = function (currentProperty) {
        if(currentProperty == "Humidity")
        {
           $scope.formData.definition = owl + "hasHumidity"; 
        }
        else if(currentProperty == "DewPoint Temperature")
        {
            $scope.formData.definition = owl + "hasHumidity"; 
        }
        else if(currentProperty == "Room Temperature")
        {
            $scope.formData.definition = owl + "hasDewPointTemperature"; 
        }
        else if(currentProperty == "Precipitation")
        {
            $scope.formData.definition = owl + "hasPrecipitation"; 
        }
        else if(currentProperty == "Wind Speed")
        {
            $scope.formData.definition = owl + "hasSpeed"; 
        }
        else if(currentProperty == "Atmospheric Pressure")
        {
            $scope.formData.definition = owl + "AtmosphericPressure"; 
        } 
    };

    $scope.collectData = function(){
        createLocation(Thing);
        createDatastream(Thing);
    };

    function createLocation(Thing){
        var jsondata = new Object(); 
            jsondata.description = "new location"; 
            jsondata.encodingType = "application/vnd.geo+json";
            jsondata.location = new Object();
            jsondata.location.type = "Point";
            jsondata.location.coordinates = "["+Thing.formData.location.lat+","+Thing.formData.location.lng+"]";

            jsondata.Thing = new Object();
            jsondata.Thing["@iot.id"] = Thing.formData.Thing_id;


        var Result = SensorThings.Location.new(jsondata);
        Thing.formData.Location_id = Result["@iot.id"];
        Thing.formData.Location_url =  serverURL + "/Locations("+Result["@iot.id"]+")";
    }

    function createDatastream(Thing){
        var jsondata = new Object(); 
            jsondata.description = $scope.formData.streamDesc; 
            jsondata.unitOfMeasurement = new Object();
            jsondata.unitOfMeasurement.symbol = "symbol";
            jsondata.unitOfMeasurement.name =  $scope.data.currentUnit;
            jsondata.unitOfMeasurement.definition = "definition";

            jsondata.observationType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement";

            jsondata.Thing = new Object();
            jsondata.Thing["@iot.id"] = Thing.formData.Thing_id;

            jsondata.Sensor = new Object();
            jsondata.Sensor.description = $scope.formData.sensorDesc;
            jsondata.Sensor.encodingType = "application/pdf";
            jsondata.Sensor.metadata = $scope.formData.sensorMeta;

            jsondata.ObservedProperty = new Object();
            jsondata.ObservedProperty.description = $scope.formData.sensorDesc;
            jsondata.ObservedProperty.name = $scope.data.currentProperty;
            jsondata.ObservedProperty.definition = $scope.formData.definition;

            var Result = SensorThings.Datastream.new(jsondata);
            Thing.formData.ObservedProperty_description = jsondata.ObservedProperty.description;
            Thing.formData.Datastream_id = Result["@iot.id"];
            Thing.formData.Datastream_url =  serverURL + "/Datastreams("+Result["@iot.id"]+")";

    }
});