angular.module('sbAdminApp').
    controller('submitControl', function($scope,Thing) {
    $scope.formData = {};
    $scope.formData.Thing_url = Thing.formData.Thing_url;
    $scope.formData.Location_url = Thing.formData.Location_url;
    $scope.formData.Datastream_url = Thing.formData.Datastream_url;

     $scope.observationState = {
        show: false
    };

    $scope.showAddObservations = function() {
        $scope.observationState.show = !$scope.observationState.show;
    };

    $scope.createOb = function(){
        createFoI($scope,Thing);
        createObservations($scope,Thing);
    };
});

function createFoI($scope,Thing){
    var jsondata = new Object(); 
        jsondata.description = $scope.formData.feaDesc; 
        jsondata.encodingType = "application/vnd.geo+json";
        jsondata.feature = new Object();
        jsondata.feature.type = "Point";
        jsondata.feature.coordinates = "[-114.06,51.05]";      

    var Result = SensorThings.FeatureOfInterest.new(jsondata);
    Thing.formData.FeatureOfInterest_id = Result["@iot.id"];
    Thing.formData.FeatureOfInterest_url =  serverURL + "/FeatureOfInterests("+Result["@iot.id"]+")";
}

function createObservations($scope,Thing){
    var jsondata = new Object(); 
        jsondata.phenomenonTime = moment().format("YYYY-MM-DDTHH:mm:ssZZ");
        jsondata.result = $scope.formData.result;
        jsondata.resultTime = "";
        
        jsondata.FeatureOfInterest = new Object();
        jsondata.FeatureOfInterest["@iot.id"] = Thing.formData.FeatureOfInterest_id;

        jsondata.Datastream = new Object();
        jsondata.Datastream["@iot.id"] = Thing.formData.Datastream_id;

    var Result = SensorThings.Observation.new(jsondata);
    Thing.formData.Observation_phenomenonTime = jsondata.phenomenonTime;
    Thing.formData.Observation_result = jsondata.result;
    Thing.formData.Observation_id = Result["@iot.id"];
    Thing.formData.Observation_url =  serverURL + "/Observations("+Result["@iot.id"]+")";
}
