angular.module('sbAdminApp').
    controller('displayControl', function($scope, Thing) {
    $scope.formData = {};
     
    // Test code
    // console.log(datastreamResult);
    // Thing.formData.Datastream_id = 14305;
    // Thing.formData.Observation_phenomenonTime = moment().format("YYYY-MM-DDTHH:mm:ssZZ");
    // Thing.formData.Observation_result = "26";
    // Thing.formData.ObservedProperty_description = "Room Temperature"
    //var datastreamResult = SensorThings.Datastream.get(Thing.formData.Datastream_id);
    // var oResult = myGet(serverURL+"/Datastreams("+Thing.formData.Datastream_id+")/Observations?$skip=4&$top=2");
    // console.log(oResult);
    // var oCount = oResult["@iot.count"];
    // console.log(oCount);
    //$scope.series = datastreamResult.description;
    // for(var ob in oResult.value)
    // {
    //     $scope.oData.push(ob.result);
    //     $scope.oLabels.push(ob.phenomenonTime);
    // }
    var formatDate = moment(Thing.formData.Observation_phenomenonTime);
    $scope.oLabels = [];
    $scope.oData = [[]];
    $scope.series = [];
    $scope.series.push(Thing.formData.ObservedProperty_description);
    $scope.oLabels.push(formatDate.hour()+":"+formatDate.minute());
    $scope.oData[0].push(Thing.formData.Observation_result);

    $scope.onChartClick = function(points, evt) {
            console.log(points, evt);
    };

    var table = $('#observations').DataTable({
        "columns": [
            { title: "Observation" }
        ],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return  "<a href='"+data+"' target='_blank'>"+data+"</a>";
                },
                "targets": 0
            }
        ]
    } );

    table.row.add([Thing.formData.Observation_url]).draw(false);

    $scope.createOb = function(){
        createObservations($scope, Thing);
        // display data on the chart
        var formatDate = moment(Thing.formData.Observation_phenomenonTime);
        $scope.oLabels.push(formatDate.hour()+":"+formatDate.minute());
        $scope.oData[0].push(Thing.formData.Observation_result);
        // display observation url on the table
        table.row.add([Thing.formData.Observation_url]).draw(false);
    };
})