// app.js
// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
  //var serverURL = "http://184.70.194.230/OGCSensorThings/v1.0";
angular.module('formApp', ['ngAnimate', 'ui.router'])
 
// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider) {
     
    $stateProvider
     
        // route to show our basic form (/form)
        .state('form', {
            url: '/form',
            templateUrl: 'form.html'
            
        })
         
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/thing)
        .state('form.thing', {
            url: '/thing',
            templateUrl: 'form-thing.html',
            controller: 'formController'
        })
         
        // url will be /form/content
        .state('form.content', {
            url: '/content',
            templateUrl: 'form-content.html',
            controller: 'contentController'
        })
         
        // url will be /form/submit
        .state('form.submit', {
            url: '/submit',
            templateUrl: 'form-submit.html',
            controller: 'submitController'
        })

        // url will be /form/display
        .state('form.display', {
            url: '/display',
            templateUrl: 'form-display.html'
        });
         
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/form/thing');



})
 
// our controller for the form
// =============================================================================
.controller('formController', function($scope) {
    var now = moment().format("YYYY-MM-DDTHH:mm:ssZZ");
    console.log(now);
    // we will store all of our form data in this object
    $scope.formData = {};

    

    $scope.create = function(desc) {
            var jsondata = new Object(); 
                jsondata.description = desc; 
            var Result = SensorThings.Thing.new(jsondata);
            $scope.formData.Thing_id = Result["@iot.id"];
            $scope.formData.Thing_url =  serverURL + "/Things("+$scope.formData.Thing_id+")";
            console.log($scope.formData.Thing_url);

    }
    // function to process the form
    /**$scope.processForm = function() {
        alert('awesome!');
    };*/
     
})

.controller('contentController', function($scope) {
    $scope.mapState = {
        show: false
    };

    $scope.dataStreamState = {
        show: false
    };




    var map;

    $scope.showMap = function() {
        $scope.mapState.show = !$scope.mapState.show;

        if(map == null)
        {
            map = new L.map('map');
            var marker = L.marker([51.079948,-114.125534]).addTo(map);
            var polygon;
            map.setView(new L.LatLng(51.079948,-114.125534), 8);
            L.tileLayer('http://{s}.tiles.mapbox.com/v3/roselin.jnb5c9am/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18
            }).addTo(map);
            map.on('click', function(e){
                marker.setLatLng(e.latlng);
                $scope.formData.location = e.latlng;
            });
        }
    }

    $scope.showAddDataStream = function() {
        $scope.dataStreamState.show = !$scope.dataStreamState.show;
    }

    $scope.allUnit = [
        {value: 0, unitName: 'Please choose unit'},
        {value: "Fahrenheit", unitName: 'Fahrenheit'},
        {value: "Celsius", unitName: 'Celsius'},
        {value: "Knot", unitName: 'Knot'},
        {value: "Kilometer", unitName: 'Kilometer'},
        {value: "Meter", unitName: 'Meter'},
        {value: "Centimeter", unitName: 'Centimeter'},
        {value: "Millimeter", unitName: 'Millimeter'},
        {value: "Mile", unitName: 'Mile'},
        {value: "Celsius", unitName: 'Celsius'},
        {value: "Inch", unitName: 'Inch'},
        {value: "Percent", unitName: 'Percent'}
    ];

    $scope.allProperty = [
        {value: 0, propertyName: 'Please choose property'},
        {value: "Humidity", propertyName: 'Humidity'},
        {value: "DewPoint Temperature", propertyName: 'DewPoint Temperature'},
        {value: "Room Temperature", propertyName: 'Room Temperature'},
        {value: "Precipitation", propertyName: 'Precipitation'},
        {value: "Wind Speed", propertyName: 'Wind Speed'},
        {value: "Atmospheric Pressure", propertyName: 'Atmospheric Pressure'}
    ];

    $scope.data = {
        currentUnit:0,
        currentProperty:0
    };

    $scope.formData.definition = "";

    var owl = "https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/WeatherOntology.owl#";
   
    $scope.switchProperty = function (currentProperty) {
        if(currentProperty == "Humidity")
        {
           $scope.formData.definition = owl + "hasHumidity"; 
        }
        else if(currentProperty == "DewPoint Temperature")
        {
            $scope.formData.definition = owl + "hasHumidity"; 
        }
        else if(currentProperty == "Room Temperature")
        {
            $scope.formData.definition = owl + "hasDewPointTemperature"; 
        }
        else if(currentProperty == "Precipitation")
        {
            $scope.formData.definition = owl + "hasPrecipitation"; 
        }
        else if(currentProperty == "Wind Speed")
        {
            $scope.formData.definition = owl + "hasSpeed"; 
        }
        else if(currentProperty == "Atmospheric Pressure")
        {
            $scope.formData.definition = owl + "AtmosphericPressure"; 
        } 
    };

    $scope.collectData = function(){
        createLocation();
        createDatastream();
    };

    function createLocation(){
        var jsondata = new Object(); 
            jsondata.description = "new location"; 
            jsondata.encodingType = "application/vnd.geo+json";
            jsondata.location = new Object();
            jsondata.location.type = "Point";
            jsondata.location.coordinates = "["+$scope.formData.location.lat+","+$scope.formData.location.lng+"]";

            jsondata.Thing = new Object();
            jsondata.Thing["@iot.id"] = $scope.formData.Thing_id;
            console.log(jsondata);

        var Result = SensorThings.Location.new(jsondata);
        $scope.formData.Location_id = Result["@iot.id"];
        $scope.formData.Location_url =  serverURL + "/Locations("+$scope.formData.Location_id+")";
    }


    function createDatastream(){
        var jsondata = new Object(); 
            jsondata.description = $scope.formData.streamDesc; 
            jsondata.unitOfMeasurement = new Object();
            jsondata.unitOfMeasurement.symbol = "symbol";
            jsondata.unitOfMeasurement.name =  $scope.data.currentUnit;
            jsondata.unitOfMeasurement.definition = "definition";

            jsondata.observationType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement";

            jsondata.Thing = new Object();
            jsondata.Thing["@iot.id"] = $scope.formData.Thing_id;

            jsondata.Sensor = new Object();
            jsondata.Sensor.description = $scope.formData.sensorDesc;
            jsondata.Sensor.encodingType = "application/pdf";
            jsondata.Sensor.metadata = $scope.formData.sensorMeta;

            jsondata.ObservedProperty = new Object();
            jsondata.ObservedProperty.description = $scope.formData.sensorDesc;
            jsondata.ObservedProperty.name = $scope.data.currentProperty;
            jsondata.ObservedProperty.definition = $scope.formData.definition;

            var Result = SensorThings.Datastream.new(jsondata);
            $scope.formData.Datastream_id = Result["@iot.id"];
            $scope.formData.Datastream_url =  serverURL + "/Datastreams("+$scope.formData.Datastream_id+")";
    }
})

.controller('submitController', function($scope) {
     $scope.observationState = {
        show: false
    };

     $scope.showAddObservations = function() {
        $scope.observationState.show = !$scope.observationState.show;
    }

    $scope.createOb = function(){
        createFoI();
        createObservations();
        
    };

    function createFoI(){
        var jsondata = new Object(); 
            jsondata.description = $scope.formData.feaDesc; 
            jsondata.encodingType = "application/vnd.geo+json";
            jsondata.feature = new Object();
            jsondata.feature.type = "Point";
            jsondata.feature.coordinates = "[-114.06,51.05]";
            
            

        var Result = SensorThings.FeatureOfInterest.new(jsondata);
        $scope.formData.FeatureOfInterest_id = Result["@iot.id"];
        $scope.formData.FeatureOfInterest_url =  serverURL + "/FeatureOfInterests("+$scope.formData.FeatureOfInterest_id+")";
    }

    function createObservations(){
        var jsondata = new Object(); 
            jsondata.phenomenonTime = "2015-09-11T00:16:20.000Z"; 
            jsondata.result = $scope.formData.result;
            jsondata.resultTime = "";
            
            jsondata.FeatureOfInterest = new Object();
            jsondata.FeatureOfInterest["@iot.id"] = $scope.formData.FeatureOfInterest_id;

            jsondata.Datastream = new Object();
            jsondata.Datastream["@iot.id"] = $scope.formData.Datastream_id;
            

        var Result = SensorThings.Observation.new(jsondata);
        $scope.formData.Observation_id = Result["@iot.id"];
        $scope.formData.Observation_url =  serverURL + "/Observations("+$scope.formData.Observation_id+")";
    }
});


