  var serverURL = "http://184.70.194.230/OGCSensorThings/v1.0";
	function myGet(url){
		var Result;
		$.ajax({
			url: url,
			type: "GET",
			dataType: 'json',
			async: false,
			success: function(data){
			Result=data;
			}
		})
		return Result;
	};
	function myPost(serverlink,jsonData){
		var Result;
        console.log(JSON.stringify(jsonData));
		$.ajax({
			type: "POST",
			url: serverlink,
			async: false,
            contentType: "application/json",
			data: JSON.stringify(jsonData),
			success: function (data){
				      Result = data;
			},
			error:function(){
                  console.log('failure');
            }
			})
		return Result;
	};
	function myDelete(serverlink){
   $.ajax({
			type: "POST",
			url: serverlink,
			async: false,
			data: {body: "" }
			}).done(function( result ) {
			console.log("Delete Successful");
			});
	};
	function myUpdate(serverlink,jsonData){
		var Result;
		alert(serverlink);
		 $.ajax({
			type: "POST",
			url: serverlink,
			async: false,
			data: JSON.stringify(jsonData),
			success: function (data){
				      Result = data;
                      console.log("Update Successfule");
			},
			error:function(){
                  alert('failure');
            }
			})
			return Result;
	};

	var SensorThings={};
    SensorThings.Thing={
            get: function(id) {
                var Result;
					Result = myGet(
					serverURL + "/Things(" + id +")"
                );
				return Result;
            },
			update: function(id, Jsondata) {
                var Result;
					Result = myUpdate(
                    serverURL + "/Things(" + id +")",Jsondata
                );
				return Result;
            },
            "new": function(Jsondata) {
                                var Result;
                                Result = myPost(
                    serverURL + "/Things",
                    Jsondata
                );
                                return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/Things(" + id +")"
                );
            },
			"select": function(key) {
                var Result;
					Result = myGet(
					serverURL + "/Things?$select=" + key 
                );
				return Result;
            }
    };

    SensorThings.Location={
            get: function(id) {
								var Result;
                Result = myGet(
								serverURL + "/Locations(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
								var Result;
                Result = myUpdate(
                    serverURL + "/Locations(" + id +")",
                    Jsondata
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;
								Result = myPost(
                    serverURL + "/Locations",
                    Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/Locations(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/Locations?$select=" + key 
                );
								return Result;
            }						
};
SensorThings.Datastream={
            get: function(id) {
								var Result;
                Result = myGet(
									serverURL + "/Datastreams(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
                var Result;
								Result = myUpdate(
                    serverURL + "/Datastreams(" + id +")",Jsondata
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;
								Result = myPost(
                    serverURL + "/Datastreams",Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/Datastreams(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/Datastreams?$select=" + key 
                );
								return Result;
            }						
};
SensorThings.Sensor={
            get: function(id) {
								var Result;
                Result = myGet(
									serverURL + "/Sensors(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
                var Result;
								Result = myUpdate(
                    serverURL + "/Sensors(" + id +")",
                    JSON.parse(Jsondata)
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;
								Result = myPost(
                    serverURL + "/Sensors",Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/Sensors(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/Sensors?$select=" + key 
                );
								return Result;
            }	 
};
SensorThings.Observation={
            get: function(id) {
								var Result;
                Result = myGet(
								serverURL + "/Observations(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
                var Result;
								Result = myUpdate(
                    serverURL + "/Observations(" + id +")",Jsondata
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;
								Result = myPost(
                    serverURL + "/Observations",
                    Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/Observations(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/Observations?$select=" + key 
                );
								return Result;
            } 
};
SensorThings.FeatureOfInterest={
            get: function(id) {
								var Result;
                Result = myGet(
									serverURL + "/FeaturesOfInterest(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
                var Result;
								Result = myUpdate(
                    serverURL + "/FeaturesOfInterest(" + id +")",
                    Jsondata
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;	
								Result = myPost(
                    serverURL + "/FeaturesOfInterest",
                    Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/FeaturesOfInterest(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/FeaturesOfInterest?$select=" + key 
                );
								return Result;
            } 
};
SensorThings.ObservedProperty={
            get: function(id) {
                var Result;
								Result = myGet(
									serverURL + "/ObservedProperties(" + id +")"
                );
								return Result;
            },
						update: function(id, Jsondata) {
                var Result;	
								Result = myUpdate(
                    serverURL + "/ObservedProperties(" + id +")",
                    JSON.parse(Jsondata)
                );
								return Result;
            },
            "new": function(Jsondata) {
								var Result;	
								Result = myPost(
                    serverURL + "/ObservedProperties",
                    Jsondata
                );
								return Result;
            },
            "delete": function(id) {
                myDelete(
                    serverURL + "/ObservedProperties(" + id +")"
                );
            },
						"select": function(key) {
                var Result;
								Result = myGet(
								serverURL + "/ObservedProperties?$select=" + key 
                );
								return Result;
            }  
};